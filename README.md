## Deep Reinforcement Learning for Algorithmic Trading

This repository contains my personal project to learn about and develop Algorithmic Trading strategies based in Deep Reinforcement Learning. My work is based in the code by [LazyProgrammer](https://github.com/lazyprogrammer/), as well as in the paper [*Practical Deep Reinforcement Learning Approach for Stock Trading*](https://arxiv.org/abs/1811.07522), by Zhuoran Xiong et al.

This first version is based on TensorFlow. It is my intention to keep developing this code in the future.

To train the model, use the command line:

```python3 RL_trader.py -m train```

To test it, use:

```python3 RL_trader.py -m test```

To plot the histogram of portfolio values use:

```python3 plot_RL_trader_rewards.py -m train```

for train mode and analogously for test mode. 

