import numpy as np
import pandas as pd

#from tensorflow.keras.models import Model
#from tensorflow.keras.layers import Dense, Input
#from tensorflow.keras.optimizers import Adam

import tensorflow

tensorflow.compat.v1.disable_eager_execution() #eager execution makes it much slower

Model = tensorflow.keras.models.Model
Dense = tensorflow.keras.layers.Dense
Input = tensorflow.keras.layers.Input
Adam = tensorflow.keras.optimizers.Adam

from datetime import datetime
import itertools
import argparse
import re
import os
import pickle

from sklearn.preprocessing import StandardScaler


def get_data():
    #returns a Tx3 list of stock prices
    #each row is a different stock
    #0 = AAPL
    #1 = MSI
    #2 = SBUX
    df = pd.read_csv('aapl_msi_sbux.csv')
    return df.values

class ReplayBuffer:
    def __init__(self, obs_dim, act_dim, size): #obs_dim is the dimension of states, act_dime the dimension of actions, size the size of the buffer
        self.obs1_buf = np.zeros([size, obs_dim], dtype = np.float32) #States
        self.obs2_buf = np.zeros([size, obs_dim], dtype = np.float32) #Next states
        self.acts_buf = np.zeros(size, dtype = np.uint8) #Actions       
        self.rews_buf = np.zeros(size, dtype = np.float32) #Rewards         
        self.done_buf = np.zeros(size, dtype = np.uint8) #Done boolean flag (is the episode over?)
        self.ptr, self.size, self.max_size = 0, 0, size #Pointer, actual size of the buffer, maximum size for the buffer      

    def store(self, obs, act, rew, next_obs, done): #This stores the new values given by a step
        self.obs1_buf[self.ptr] = obs #
        self.obs2_buf[self.ptr] = next_obs #Next states
        self.acts_buf[self.ptr] = act #Actions       
        self.rews_buf[self.ptr] = rew #Rewards         
        self.done_buf[self.ptr] = done
        self.ptr = (self.ptr + 1) % self.max_size
        self.size = min(self.size + 1, self.max_size)      

    def sample_batch(self, batch_size = 32): #Chooses batch_size random indices and returns a dictionary containing the respective elements of the buffer
        idxs = np.random.randint(0, self.size, size = batch_size)
        return dict(s = self.obs1_buf[idxs],
                    s2 = self.obs2_buf[idxs],
                    a = self.acts_buf[idxs],
                    r = self.rews_buf[idxs],
                    d = self.done_buf[idxs])



def get_scaler(env): #Get a sklearn scaler for states
    states = []
    for _ in range(env.n_step):
        action = np.random.choice(env.action_space)
        state, reward, done, info = env.step(action)
        states.append(state)
        if done:
            break 

    scaler = StandardScaler()
    scaler.fit(states)
    return scaler

def maybe_make_dir(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)

def mlp(input_dim, n_action, n_hidden_layers = 1, hidden_dim = 32): #Create a NN model
    i = Input(shape = (input_dim, ))
    x = i

    for _ in range(n_hidden_layers):
        x = Dense(units = hidden_dim, activation = 'relu')(x)

    x = Dense(units = n_action)(x)  

    model = Model(i, x)

    model.compile(loss = 'mse', optimizer = 'adam', run_eagerly = False)

    print((model.summary()))
    return model


class MultiStockEnv:
    #An n-stock trading environment, states are 2*n_stock + 1 dimensional vectors: The first n_stock values are the shares of each stock owned, the second n_stock are their current prices and the last value is cash. Actions are n_stock^3: for each stock you can buy, sell or hold it (in its entirety)
    def __init__(self, data, initial_investment = 20000): #data contains the multi-valued time series of prices
        self.stock_price_history = data 
        self.n_step, self.n_stock = self.stock_price_history.shape

        self.initial_investment = initial_investment
        self.cur_step = None #current state
        self.stock_owned = None 
        self.stock_price = None
        self.cash_in_hand = None

        self.action_space = np.arange(3**self.n_stock) #3 actions: buy, sell and hold, this for n_stock stock
        
        self.action_list = list(map(list, itertools.product([0, 1, 2], repeat = self.n_stock)))

        self.state_dim = self.n_stock * 2 + 1

        self.reset()

    def reset(self): #resets atributes of environment
        self.cur_step = 0
        self.stock_owned = np.zeros(self.n_stock)
        self.stock_price = self.stock_price_history[self.cur_step]
        self.cash_in_hand = self.initial_investment
        return self._get_obs()

    def step(self, action): #performs the action in the environment
        assert action in self.action_space #check if action exists in the action_space

        prev_val = self._get_val() #the current values of the portfolio

        self.cur_step += 1 #update current step
        self.stock_price = self.stock_price_history[self.cur_step] #update prices

        self._trade(action) #do the trading
        cur_val = self._get_val() #get the values of the portfolio after action has been taken
        reward = cur_val - prev_val #the reward is the difference in the portfolio
        done = self.cur_step == self.n_step - 1 #update done boolean flag to know if the episode is finished
        info = {'cur_val': cur_val} #current value directory
        return self._get_obs(), reward, done, info

    def _get_obs(self): #the states/portfolio configurations
        obs = np.empty(self.state_dim)
        obs[: self.n_stock] = self.stock_owned 
        obs[self.n_stock: 2*self.n_stock] = self.stock_price         
        obs[-1] = self.cash_in_hand
        return obs
    
    def _get_val(self): #value of portfolio
        return self.stock_owned.dot(self.stock_price) + self.cash_in_hand 
    
    def _trade(self, action): #action is an integer indexing actions in action_list
        action_vec = self.action_list[action] #get the action as a n_stock-tuple of instructions: for each stock, buy, sell or hold
        sell_index = []
        buy_index = []
        for i, a in enumerate(action_vec):
            if a == 0:
                sell_index.append(i)
            elif a == 2:
                buy_index.append(i)

        if sell_index:
            for i in sell_index: 
                self.cash_in_hand += self.stock_price[i]*self.stock_owned[i]
                self.stock_owned[i] = 0

        if buy_index: #we buy one share per time until we run out of cash
            can_buy = True
            while can_buy:
                for i in buy_index:
                    if self.cash_in_hand > self.stock_price[i]:
                        self.stock_owned[i] += 1
                        self.cash_in_hand -= self.stock_price[i]
                    else:
                        can_buy = False

class DQNAgent(object):
    def __init__(self, state_size, action_size):
        self.state_size = state_size
        self.action_size = action_size
        self.memory = ReplayBuffer(state_size, action_size, size = 500)
        self.gamma = 0.95 #discount rate
        self.epsilon =  1
        self.epsilon_min = 0.01
        self.epsilon_decay = 0.995
        self.model = mlp(input_dim = state_size, n_action = action_size)
    
    def update_replay_memory(self, state, action, reward, next_state, done):
        self.memory.store(state, action, reward, next_state, done)

    def act(self, state):
        if np.random.rand() <= self.epsilon: #it gives a chance to perform a random action to try something new
            return np.random.choice(self.action_size)
        act_values = self.model.predict(state)
        return np.argmax(act_values[0]) #predicted action through our model
    
    def replay(self, batch_size = 32): #this function performs the learning
        if self.memory.size < batch_size:
            return 
        minibatch = self.memory.sample_batch(batch_size) #take a batch of samples, remember memory is a ReplayBuffer instance  
        states = minibatch['s']
        actions = minibatch['a']
        rewards = minibatch['r']
        next_states = minibatch['s2']
        done = minibatch['d'] 

        #this actualizes the targets in a gradient-descent-like process
        target = rewards + self.gamma * np.amax(self.model.predict(next_states), axis = 1)
        target[done] = rewards[done] #for the actions taken at the end of an episode

        target_full = self.model.predict(states)
        target_full[np.arange(batch_size), actions] = target #thus we only update the values of the target for actions actually taken, so the others have no effect in training the model

        self.model.train_on_batch(states, target_full) #train the model
        #self.model.fit(states, target_full, verbose = 0) #train the model

        if self.epsilon > self.epsilon_min: #update epsilon if it is not already the previously set minimum
            self.epsilon *= self.epsilon_decay
    
    def load(self, name):
        self.model.load_weights(name)
    
    def save(self, name):
        self.model.save_weights(name)

def play_one_episode(agent, env, is_train):
    state = env.reset() #reset environment
    state = scaler.transform([state])
    done = False

    while not done:
        action = agent.act(state)  #find the action to perform
        next_state, reward, done, info = env.step(action) #perform the action 
        next_state = scaler.transform([next_state])
        if is_train == 'train': #check if we are in train mode
            agent.update_replay_memory(state, action, reward, next_state, done)
            agent.replay(batch_size)
        state = next_state #update state and continue while loop

    return info['cur_val'] #return current value of portfolio   

if __name__ == '__main__':
    models_folder = 'rl_trader_models'
    rewards_folder = 'rl_trader_rewards'
    num_episodes = 2000
    batch_size = 32 
    initial_investment = 20000

    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--mode', type = str, required = True,
                        help = "either 'train' or 'test'") 
    args = parser.parse_args()

    maybe_make_dir(models_folder)
    maybe_make_dir(rewards_folder)

    data = get_data()
    n_timesteps, nstocks = data.shape

    n_train = n_timesteps // 2

    train_data = data[: n_train]
    test_data = data[n_train: ]

    env = MultiStockEnv(train_data, initial_investment)
    state_size = env.state_dim
    action_size = len(env.action_space)
    agent = DQNAgent(state_size, action_size)   
    scaler = get_scaler(env)

    portfolio_value = []
    if args.mode == 'test':
        with open(f'{models_folder}/scaler.pkl', 'rb') as f: #get the scaler previously generated when in train mode
            scaler = pickle.load(f)

        env = MultiStockEnv(test_data, initial_investment) #use the environment generated by test data
        agent.epsilon = 0.01 #default is 1, so make sure to set it equal to a small value

        agent.load(f'{models_folder}/dqn.h5') #load weights of trained model

    for e in range(num_episodes): #play de tarding game num_episodes times
        t0 = datetime.now()
        val = play_one_episode(agent, env, args.mode)
        dt = datetime.now() - t0 #duration of the episode
        print(f'episode: {e+1}/{num_episodes}, episode end value: {val: .2f}, duration: {dt}')
        portfolio_value.append(val)

    if args.mode == 'train':
        agent.save(f'{models_folder}/dqn.h5') #save the weigths of the trained model

        with open(f'{models_folder}/scaler.pkl', 'wb') as f: #save the scaler
            pickle.dump(scaler, f)
    
    np.save(f'{rewards_folder}/{args.mode}.npy', portfolio_value) #for both test and train modes, save the portfolio values 



   


 
             
