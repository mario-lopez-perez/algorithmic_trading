import matplotlib.pyplot as plt
import argparse
import numpy as np

rewards_folder = 'rl_trader_rewards'

parser = argparse.ArgumentParser()
parser.add_argument('-m', '--mode', type = str, required = True,
                        help = "either 'train' or 'test'") 
args = parser.parse_args()

np.load(f'{rewards_folder}/{args.mode}.npy') #load the portfolio values 

print(f'average reward: {a.mean(): .2f}, min: {a.min(): .2f}, max : {a.max(): .2f}')
plt.hist(a, bins = 20)
plt.title(args.mode)
plt.show()